variable "ssh_credentials" {
  description = "Credentials for connect to instances"
  type = object({
    user        = string
    private_key = string
  })
  default = {
    user        = "ubuntu"
    private_key = "/home/petr/.ssh/id_rsa"
  }
}
