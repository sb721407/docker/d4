##### IP address #####

resource "yandex_vpc_address" "addr" {
  name = "static-ip"
  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_dns_recordset" "dns_name" {
  depends_on = [yandex_vpc_address.addr]
  zone_id    = local.local_data.zone_id
  name       = "registry"
  type       = "A"
  ttl        = 200
  data       = [yandex_vpc_address.addr.external_ipv4_address[0].address]
}

##### VMs Part #####

resource "yandex_compute_instance" "ubuntu" {

  name        = "ubuntu"
  platform_id = "standard-v2"
  #  zone        = "ru-central1-a"
  
  resources {
    core_fraction = 100
    cores         = "4"
    memory        = "4"
  }

  boot_disk {
    initialize_params {
#      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd8earpjmhevh8h6ug5o" # ubuntu-22
      size     = 10
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
    nat_ip_address = yandex_vpc_address.addr.external_ipv4_address[0].address
  }

  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }

  scheduling_policy {
    preemptible = true
  }
  
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[all]
${yandex_compute_instance.ubuntu.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

EOF
  filename = "${path.module}/inventory"
}

##### Provisioning #####

resource "null_resource" "ubuntu" {
  depends_on = [yandex_compute_instance.ubuntu, local_file.inventory]

  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.ubuntu.network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ~/.ssh/id_rsa playbook.yml"
  }
}
