## SkillBox DevOps. Docker. Часть 4. Дополнительные инструменты для работы с Docker

SkillBox DevOps. Docker. Часть 4. Дополнительные инструменты для работы с Docker

--------------------------------------------------------

Сценарий работы с репозиторием:
```
cd existing_repo
git remote add origin git@gitlab.com:sb721407/docker/d4.git
git branch -M main
git push -uf origin main
```

### Скрины запущенных контейнеров в Docker Registry:

![Screenshot3](png/screen_3.png?raw=true "Docker Registry")

![Screenshot1](png/screen_1.png?raw=true "Successful Login")

### Docker-файл с вашим образом:

https://gitlab.com/sb721407/docker/d4/-/blob/main/Dockerfile

### Скрины созданных образов:

![Screenshot2](png/screen_2.png?raw=true "My images")

### Скрин успешного пуша в Docker Registry:

![Screenshot2](png/screen_2.png?raw=true "Push to my Docker Registry")

### Скрин успешного пуша в Docker Hub:

![Screenshot5](png/screen_5.png?raw=true "Push to Docker Hub")

### Опционально: скрины с процессом подписи:

![Screenshot4](png/screen_4.png?raw=true "Sign and Push")



